<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("application/libraries/REST_Controller.php");
require_once("application/libraries/Format.php");

class UsuarioPago extends REST_Controller{

  public function __construct() {
        parent::__construct();
  }

  public function isUser($codUser){
    $this->load->model("md_usuario");
    return $this->md_usuario->isUser($codUser);
  }

  public function isPag($codPag){
    $this->load->model("md_pago");
    return $this->md_pago->isPago($codPag);
  }

  public function mapping($tipo){
    return array(
                  "codigousuario" => $this->$tipo("codigousuario"),
                  "codigopago" => $this->$tipo("codigopago")
                );
  }

  public function usuarioPagoAgregar_put(){
    $this->load->model("md_usuarioPago");
    if(!$this->isUser($this->put("codigousuario")))
      $this->set_response(["Usuario no registrado"], REST_Controller::HTTP_BAD_REQUEST);
    else
      if(!$this->isPag($this->put("codigopago")))
        $this->set_response(["Pago no registrado"], REST_Controller::HTTP_BAD_REQUEST);
      else
        $this->set_response($this->md_usuarioPago->save($this->mapping("put")), REST_Controller::HTTP_CREATED);
  }


  public function usuarioPagoBorrar_delete($idUser, $idPago){
    $this->load->model("md_usuarioPago");
    $this->set_response($this->md_usuarioPago->delete($idUser, $idPago), REST_Controller::HTTP_OK);
  }

}
