<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("application/libraries/REST_Controller.php");
require_once("application/libraries/Format.php");

class Usuario extends REST_Controller{
  private $edad;
  private $return;

  public function __construct() {
        parent::__construct();
        $this->edad = 18;
        $this->return = [];
  }

  public function checkName($name){
    if(!empty($name))
      return false;
    else{
      $this->return[] = "El nombre no puede ser vacio";
      return true;
    }
  }

  public function checkEdad($edad){
    if($edad > $this->edad)
      return true;
    else{
      $this->return[] = "La edad debe ser mayor a 18";
      return false;
    }
  }

  public function mapping($tipo){
    return array(
                  "usuario" => $this->$tipo("usuario"),
                  "clave" => $this->$tipo("clave"),
                  "edad" => $this->$tipo("edad")
                );
  }

  function usuarioAgregar_put()
  {
      $this->load->model("md_usuario");
      if($this->checkName($this->put("usuario")) || !$this->checkEdad($this->put("edad")))
        $this->set_response($this->return, REST_Controller::HTTP_BAD_REQUEST);
      else
        $this->set_response($this->md_usuario->save($this->mapping("put")), REST_Controller::HTTP_CREATED);
  }

  function usuarioModificar_post($id)
  {
       $this->load->model("md_usuario");
       if($this->checkName($this->post("usuario")) || !$this->checkEdad($this->post("edad")))
         $this->set_response($this->return, REST_Controller::HTTP_BAD_REQUEST);
       else
         $this->set_response($this->md_usuario->update($id,$this->mapping("post")), REST_Controller::HTTP_OK);
  }

  function usuarioBorrar_delete($id)
  {
     // Para borrar un recurso
     $this->load->model("md_usuario");
     $this->set_response($this->md_usuario->delete($id), REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
  }

}
