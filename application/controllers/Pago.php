<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("application/libraries/REST_Controller.php");
require_once("application/libraries/Format.php");

class Pago extends REST_Controller{

  public function __construct() {
    parent::__construct();
  }


  public function mapping($tipo){
      return array(
                    "importe" => $this->$tipo("importe"),
                    "fecha" => $this->$tipo("fecha")
                  );
    }

  public function checkImporte($importe){
    if($importe > 0)
      return false;
    else{
      $this->return[] = "El importe debe ser mayor a cero (00)";
      return true;
    }
  }

  public function checkDate($date){
    if(strtotime($date) >= strtotime(date("Y-m-d")))
      return true;
    else{
      $this->return[] = "La fecha debe ser mayor a la fecha actual";
      return false;
    }
  }

  function pagoAgregar_put()
  {
      $this->load->model("md_pago");
      if($this->checkImporte($this->put("importe")) || !$this->checkDate($this->put("fecha")))
        $this->set_response($this->return, REST_Controller::HTTP_BAD_REQUEST);
      else
        $this->set_response($this->md_pago->save($this->mapping("put")), REST_Controller::HTTP_CREATED);
  }

  function pagoModificar_post($id)
  {
       $this->load->model("md_pago");
         if($this->checkImporte($this->post("importe")) || !$this->checkDate($this->post("fecha")))
         $this->set_response($this->return, REST_Controller::HTTP_BAD_REQUEST);
       else
         $this->set_response($this->md_pago->update($id, $this->mapping("post")), REST_Controller::HTTP_OK);
  }

  function pagooBorrar_delete($id)
  {     
     $this->load->model("md_pago");
     $this->set_response($this->md_pago->delete($id), REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
  }

}
