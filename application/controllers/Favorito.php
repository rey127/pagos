<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("application/libraries/REST_Controller.php");
require_once("application/libraries/Format.php");

class Favorito extends REST_Controller{

  public function __construct() {
        parent::__construct();
  }

  public function isUser($codUser){
    $this->load->model("md_usuario");
    return $this->md_usuario->isUser($codUser);
  }


    public function mapping($tipo){
      return array(
                    "codigousuario" => $this->$tipo("codigousuario"),
                    "codigousuariofavorito" => $this->$tipo("codigousuariofavorito")
                  );
    }

  public function favoritoAgregar_put(){
    $this->load->model("md_favorito");
    if($this->isUser($this->put("codigousuario")) && $this->isUser($this->put("codigousuariofavorito")))
      $this->set_response($this->md_favorito->save($this->mapping("put")), REST_Controller::HTTP_CREATED);
    else
      $this->set_response(["Usuario no registrado"], REST_Controller::HTTP_BAD_REQUEST);
  }


  public function favoritoBorrar_delete($id, $idFav){
    $this->load->model("md_favorito");
    $this->set_response($this->md_favorito->delete($id, $idFav), REST_Controller::HTTP_OK);
  }

}
