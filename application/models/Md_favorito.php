<?php
  class Md_favorito extends CI_Model{
    private $table = "favorito";

    public function __construct() {
          parent::__construct();
    }

    public function save($data){
        $this->db->insert($this->table, $data);
        return $data;
    }


    public function delete($id,$idFav){
      $this->db->where("codigousuario", $id);
      $this->db->where("codigousuariofavorito", $idFav);
      $this->db->delete($this->table);
      return $this->db->affected_rows();
    }

  }
