<?php
  class Md_usuarioPago extends CI_Model{
    private $table = "usuarioPago";

    public function __construct() {
          parent::__construct();
    }

    public function save($data){
        $this->db->insert($this->table, $data);
        return $data;
    }


    public function delete($idUser, $idPag){
      $this->db->where("codigousuario", $idUser);
      $this->db->where("codigopago", $idPag);
      $this->db->delete($this->table);
      return $this->db->affected_rows();
    }

  }
