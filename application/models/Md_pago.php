 <?php 
  class Md_pago extends CI_Model {
    private $table = "pago";
    private $key = "codigopago";

    public function __construct() {
       parent::__construct();
    }

    public function isPago($codPag){
      $this->db->select("*");
      $this->db->from($this->table);
      $this->db->where($this->key, $codPag);
      $query = $this->db->get();
      if($query->num_rows() > 0)
        return true;
      else
        return false;
    }

    public function save($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $array){
      $this->db->set($array);
      $this->db->where($this->key, $id);
      $this->db->update($this->table);
      return $this->db->affected_rows();
    }

    public function delete($id){
      $this->db->where($this->key, $id);
      $this->db->delete($this->table);
      return $this->db->affected_rows();
    }

  }
