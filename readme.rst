###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `CodeIgniter Downloads
<https://codeigniter.com/download>`_ page.

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Resources
*********

-  `User Guide <https://codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community IRC <https://webchat.freenode.net/?channels=%23codeigniter>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_
or via our `page on HackerOne <https://hackerone.com/codeigniter>`_, thank you.

***************
Acknowledgement
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.

NOTA
    Se deberá tener importada la base de datos db_pagos que se encuentra en el repositorio actual y contar con un servidor

Rutas API para USUARIO
--------------------------
 - POST
        1. localhost/pagos/usuarioModificar
            Objeto de Entrada:
                {
                    "usuario" : "[string]",
                    "clave" : "[string]",
                    "edad" : "[integer]"
                }
        
- PUT
        1. localhost/pagos/usuarioAgregar
            Objeto de Entrada:
                {
                    "usuario" : "[string]",
                    "clave" : "[string]",
                    "edad" : "[integer]"
                }

- DELETE
        1. localhost/pagos/usuarioBorrar/[id a eliminar]


Rutas API para PAGO
--------------------------
 - POST
        1. localhost/pagos/pagoModificar
            Objeto de Entrada:
                {
                    "importe" : "[double]",
                    "fecha" : "[date]"
                }
        
- PUT
        1. localhost/pagos/pagoAgregar
            Objeto de Entrada:
                {
                    "importe" : "[double]",
                    "fecha" : "[date]"
                }

- DELETE
        1. localhost/pagos/pagooBorrar/[id a eliminar]
        
        
Rutas API para FAVORITO
--------------------------

- PUT
        1. localhost/pagos/favoritoAgregar
            Objeto de Entrada:
                {
                    "codigousuario" : "[integer]",
                    "codigousuariofavorito" : "[integer]"
                }

- DELETE
        1. localhost/pagos/favoritoBorrar/[id a eliminar]
        
Rutas API para Usuario Pago
--------------------------

- PUT
        1. localhost/pagos/usuarioPagoAgregar
            Objeto de Entrada:
                {
                    "codigopago" : "[integer]",
                    "codigousuario" : "[integer]"
                }

- DELETE
        1. localhost/pagos/usuarioPagoBorrar/[id a eliminar]





